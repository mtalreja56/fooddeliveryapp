package com.fooddeliveryapp.callback;

import com.fooddeliveryapp.model.Items;

public interface AddItemCallback {

    void updateItem(Items item,int type);
}
