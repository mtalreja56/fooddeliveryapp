package com.fooddeliveryapp.callback;

import com.fooddeliveryapp.model.Items;

import java.util.List;

public interface ItemCallback {

    void getAllItems(List<Items> mItems);
}
