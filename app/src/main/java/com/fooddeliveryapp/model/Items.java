package com.fooddeliveryapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;


@Entity(tableName = "Items")
public class Items {

    @PrimaryKey(autoGenerate = true)
    private int key;
    @ColumnInfo(name = "item_name")
    private String itemName;
    @ColumnInfo(name = "item_image")
    private int itemImage;
    @ColumnInfo(name = "item_price")
    private List<Double> itemPrice;
    @ColumnInfo(name = "item_current_amount")
    private Double itemCurrentAmount;
    @ColumnInfo(name = "item_quantity")
    private String itemQuantity;
    @ColumnInfo(name = "is_added")
    private boolean isAdded;

    public Items(String itemName, int itemImage, List<Double> itemPrice, String itemQuantity, boolean isAdded) {
        this.itemName = itemName;
        this.itemImage = itemImage;
        this.itemPrice = itemPrice;
        this.itemQuantity = itemQuantity;
        this.isAdded = isAdded;
    }

    public Double getItemCurrentAmount() {
        return itemCurrentAmount;
    }

    public void setItemCurrentAmount(Double itemCurrentAmount) {
        this.itemCurrentAmount = itemCurrentAmount;
    }

    public String getItemQuantity() {
        return itemQuantity;
    }

    public String getItemName() {
        return itemName;
    }

    public int getItemImage() {
        return itemImage;
    }

    public List<Double> getItemPrice() {
        return itemPrice;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public void setAdded(boolean added) {
        isAdded = added;
    }

    @Override
    public String toString() {
        return "Items{" +
                "itemName='" + itemName + '\'' +
                ", itemImage='" + itemImage + '\'' +
                ", itemPrice=" + itemPrice +
                ", isAdded=" + isAdded +
                '}';
    }
}
