package com.fooddeliveryapp.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fooddeliveryapp.R;
import com.fooddeliveryapp.adapter.FoodItemAdapter;
import com.fooddeliveryapp.callback.ItemCallback;
import com.fooddeliveryapp.db.DatabaseManager;
import com.fooddeliveryapp.model.Items;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DeliveryStatusActivity extends AppCompatActivity implements ItemCallback, OnMapReadyCallback {

    private RecyclerView deliveryRecyclerView;
    private double latitude, longitude;
    private TextView distanceBetween;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, DeliveryStatusActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getStoredLocation();
        setContentView(R.layout.activity_delivery_status);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Objects.requireNonNull(mapFragment).getMapAsync(this);
        bindViews();
    }

    private void bindViews() {
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(getString(R.string.delivery_status_tag));
        distanceBetween = findViewById(R.id.distance_between);
        DatabaseManager.getInstance(this).getAllItems(this);
        deliveryRecyclerView = findViewById(R.id.delivery_recycler_view);
        deliveryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Button backToFood = findViewById(R.id.back_to_food_items);
        backToFood.setOnClickListener(view -> {
            startActivity(new Intent(this, FoodOrderActivity.class));
            finish();
        });
    }

    @Override
    public void getAllItems(List<Items> mItems) {
        List<Items> itemsList = new ArrayList<>();
        for (int i = 0; i < mItems.size(); i++) {
            if (mItems.get(i).isAdded()) {
                itemsList.add(mItems.get(i));
            }
        }
        FoodItemAdapter mAdapter = new FoodItemAdapter(itemsList, (item, type) ->
                DatabaseManager.getInstance(this).updateDeliveryStatus(item), 2);
        deliveryRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        List<Marker> markersList = new ArrayList<>();
        Location usersLocation = new Location("Users Location");
        usersLocation.setLatitude(latitude);
        usersLocation.setLongitude(longitude);
        Location deliveryLocation = new Location("Delivery Boy");
        deliveryLocation.setLatitude(19.0178);
        deliveryLocation.setLongitude(72.8478);
        Marker usersMarker = googleMap.addMarker(new MarkerOptions().
                position(new LatLng(usersLocation.getLatitude(), usersLocation.getLongitude())).title("Delivery Address"));
        Marker deliveryMaker = googleMap.addMarker(new MarkerOptions().
                position(new LatLng(deliveryLocation.getLatitude(), deliveryLocation.getLongitude())).title("Delivery Boy"));
        double distance = usersLocation.distanceTo(deliveryLocation) / 1000;
        markersList.add(usersMarker);
        markersList.add(deliveryMaker);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }
        int padding = 50;
        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        googleMap.setOnMapLoadedCallback(() -> googleMap.animateCamera(cu));
        distanceBetween.setText(String.format("%s KM AWAY ", new DecimalFormat("##.##").format(distance)));
    }

    private void getStoredLocation() {
        SharedPreferences mSharedPreference = getSharedPreferences("user_location", Context.MODE_PRIVATE);
        latitude = Double.longBitsToDouble(mSharedPreference.getLong("latitude", 0));
        longitude = Double.longBitsToDouble(mSharedPreference.getLong("longitude", 0));
    }
}
