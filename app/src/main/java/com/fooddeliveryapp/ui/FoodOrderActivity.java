package com.fooddeliveryapp.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fooddeliveryapp.R;
import com.fooddeliveryapp.adapter.FoodItemAdapter;
import com.fooddeliveryapp.callback.ItemCallback;
import com.fooddeliveryapp.db.DatabaseManager;
import com.fooddeliveryapp.model.Items;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FoodOrderActivity extends AppCompatActivity implements ItemCallback, LocationListener {

    private static final int LOCATION_CODE = 101;
    private RecyclerView itemsRecyclerView;
    private LocationManager locationManager;
    private Geocoder geocoder;
    private TextView yourLocation;
    private FoodItemAdapter mAdapter;
    private List<Items> mItemList;
    private SharedPreferences mSharedPreferences;
    private int count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DatabaseManager.getInstance(this).setItems(getItems());
        setContentView(R.layout.activity_food_order);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkLocationPermission();
        bindViews();
    }

    private void bindViews() {
        mSharedPreferences = getSharedPreferences("user_location", Context.MODE_PRIVATE);
        geocoder = new Geocoder(this, Locale.getDefault());
        DatabaseManager.getInstance(this).getAllItems(this);
        itemsRecyclerView = findViewById(R.id.items_recycler_view);
        yourLocation = findViewById(R.id.your_location);
        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(getString(R.string.food_items_tag));
        Button admin_panel_button = findViewById(R.id.admin_panel_button);
        admin_panel_button.setOnClickListener(view -> {
            startActivity(DeliveryStatusActivity.getStartIntent(this));
            finish();
        });
    }

    @Override
    public void getAllItems(List<Items> mItems) {
        this.mItemList=mItems;
         mAdapter = new FoodItemAdapter(mItems, (item, type) ->
                DatabaseManager.getInstance(this).updateDeliveryStatus(item), 1);
        itemsRecyclerView.setAdapter(mAdapter);
        updateDataEveryMinute();
    }

    private List<Items> getItems() {
        List<Items> mItems = new ArrayList<>();
        mItems.add(new Items("Bread", R.drawable.ic_bread, getPriceList1(), "6 packets", false));
        mItems.add(new Items("Bagels", R.drawable.ic_bagels, getPriceList2(), "1 kg", false));
        mItems.add(new Items("Pasta", R.drawable.ic_pasta, getPriceList3(), "1 kg", false));
        mItems.add(new Items("Cereals", R.drawable.ic_cereal, getPriceList4(), "1 packet", false));
        mItems.add(new Items("Buns", R.drawable.ic_buns, getPriceList5(), "4 packet", false));
        mItems.add(new Items("Carrot", R.drawable.ic_carrot, getPriceList6(), "1 kg", false));
        mItems.add(new Items("Mushrooms", R.drawable.ic_mushroom, getPriceList7(), "1 kg", false));
        mItems.add(new Items("Cucumber", R.drawable.ic_cucumber, getPriceList8(), "1 kg", false));
        mItems.add(new Items("Apples", R.drawable.ic_apple, getPriceList9(), "1 kg", false));
        mItems.add(new Items("Watermelon", R.drawable.ic_watermelon, getPriceList10(), "1 kg", false));
        mItems.add(new Items("Banana", R.drawable.ic_banana, getPriceList11(), "1 dozen", false));
        mItems.add(new Items("Peanut Butter", R.drawable.ic_peanut_butter, getPriceList12(), "1 bottle", false));
        return mItems;
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_CODE);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

            } else {
                requestLocationPermission();
            }
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putLong("latitude", Double.doubleToLongBits(location.getLatitude()));
        mEditor.putLong("longitude", Double.doubleToLongBits(location.getLongitude()));
        mEditor.apply();
        try {
            List<Address> mAdressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            yourLocation.setText(String.format("%s %2s", "Your Location - ", mAdressList.get(0).getAddressLine(0)));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void updateDataEveryMinute()
    {
        final Handler handler = new Handler();
        handler.postDelayed( new Runnable() {
            @Override
            public void run() {
                if(count==mItemList.get(0).getItemPrice().size()-1)
                {
                    count = -1;
                }
               for(int i=0;i<mItemList.size();i++)
               {
                    mItemList.get(i).setItemCurrentAmount(mItemList.get(i).getItemPrice().get(count));
               }
                mAdapter.setItemList(mItemList);
                mAdapter.notifyDataSetChanged();
                handler.postDelayed( this, 60 * 1000 );
                count++;
            }
        }, 0);
    }

    private List<Double> getPriceList1()
    {
        List<Double> price = new ArrayList<>();
        price.add(100.0);
        price.add(200.0);
        price.add(150.0);
        price.add(250.0);
        price.add(70.0);
        price.add(130.0);
        return price;
    }

    private List<Double> getPriceList2()
    {
        List<Double> price = new ArrayList<>();
        price.add(200.0);
        price.add(50.0);
        price.add(70.0);
        price.add(85.0);
        price.add(120.0);
        price.add(300.0);
        return price;
    }

    private List<Double> getPriceList3()
    {
        List<Double> price = new ArrayList<>();
        price.add(80.0);
        price.add(150.0);
        price.add(340.0);
        price.add(180.0);
        price.add(55.0);
        price.add(75.0);
        return price;
    }

    private List<Double> getPriceList4()
    {
        List<Double> price = new ArrayList<>();
        price.add(100.0);
        price.add(200.0);
        price.add(150.0);
        price.add(250.0);
        price.add(70.0);
        price.add(130.0);
        return price;
    }

    private List<Double> getPriceList5()
    {
        List<Double> price = new ArrayList<>();
        price.add(100.0);
        price.add(200.0);
        price.add(150.0);
        price.add(250.0);
        price.add(70.0);
        price.add(130.0);
        return price;
    }

    private List<Double> getPriceList6()
    {
        List<Double> price = new ArrayList<>();
        price.add(100.0);
        price.add(200.0);
        price.add(150.0);
        price.add(250.0);
        price.add(70.0);
        price.add(130.0);
        return price;
    }

    private List<Double> getPriceList7()
    {
        List<Double> price = new ArrayList<>();
        price.add(100.0);
        price.add(200.0);
        price.add(150.0);
        price.add(250.0);
        price.add(70.0);
        price.add(130.0);
        return price;
    }

    private List<Double> getPriceList8()
    {
        List<Double> price = new ArrayList<>();
        price.add(100.0);
        price.add(200.0);
        price.add(150.0);
        price.add(250.0);
        price.add(70.0);
        price.add(130.0);
        return price;
    }

    private List<Double> getPriceList9()
    {
        List<Double> price = new ArrayList<>();
        price.add(400.0);
        price.add(110.0);
        price.add(35.0);
        price.add(125.0);
        price.add(444.0);
        price.add(343.0);
        return price;
    }
    private List<Double> getPriceList10()
    {
        List<Double> price = new ArrayList<>();
        price.add(74.0);
        price.add(320.0);
        price.add(232.0);
        price.add(454.0);
        price.add(212.0);
        price.add(32.0);
        return price;
    }
    private List<Double> getPriceList11()
    {
        List<Double> price = new ArrayList<>();
        price.add(320.0);
        price.add(55.0);
        price.add(95.0);
        price.add(125.0);
        price.add(175.0);
        price.add(230.0);
        return price;
    }
    private List<Double> getPriceList12()
    {
        List<Double> price = new ArrayList<>();
        price.add(430.0);
        price.add(33.0);
        price.add(354.0);
        price.add(333.0);
        price.add(454.0);
        price.add(222.0);
        return price;
    }
}
