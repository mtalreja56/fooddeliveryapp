package com.fooddeliveryapp.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.fooddeliveryapp.model.Items;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface ItemDao {

    @Query("select  * from Items")
    Single<List<Items>> getAllItems();

    @Insert
    Single<List<Long>> setAllItems(List<Items> items);

    @Update
    Single<Integer> updateDeliveryStatus(Items items);
}
