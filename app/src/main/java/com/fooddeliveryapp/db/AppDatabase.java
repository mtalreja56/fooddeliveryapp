package com.fooddeliveryapp.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.fooddeliveryapp.utils.AmountTypeConverter;
import com.fooddeliveryapp.model.Items;


@Database(entities = Items.class, version = 1,exportSchema = false)
@TypeConverters({AmountTypeConverter.class})
abstract class AppDatabase extends RoomDatabase {
    abstract ItemDao itemDao();
}
