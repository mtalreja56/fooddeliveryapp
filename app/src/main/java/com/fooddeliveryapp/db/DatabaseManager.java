package com.fooddeliveryapp.db;

import android.content.Context;

import androidx.room.Room;

import com.fooddeliveryapp.callback.ItemCallback;
import com.fooddeliveryapp.model.Items;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class DatabaseManager {

    private static DatabaseManager dbManager;
    private AppDatabase mAppDatabase;

    public static DatabaseManager getInstance(Context context) {
        if (dbManager == null) {
            dbManager = new DatabaseManager(context);
        }
        return dbManager;
    }


    private DatabaseManager(Context mContext) {
        mAppDatabase = Room.databaseBuilder(mContext, AppDatabase.class, "foodDelivery").build();
    }

    public void getAllItems(final ItemCallback itemCallback) {
        CompositeDisposable mDisposable = new CompositeDisposable();
        mDisposable.add(
                mAppDatabase.itemDao().getAllItems()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(itemCallback::getAllItems));
    }

    public void setItems(List<Items> items) {
        mAppDatabase.itemDao().setAllItems(items).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public void updateDeliveryStatus(Items item) {
        mAppDatabase.itemDao().updateDeliveryStatus(item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }


}
