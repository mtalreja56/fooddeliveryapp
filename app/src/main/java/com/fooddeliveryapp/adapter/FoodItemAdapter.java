package com.fooddeliveryapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fooddeliveryapp.R;
import com.fooddeliveryapp.callback.AddItemCallback;
import com.fooddeliveryapp.model.Items;

import java.util.List;

public class FoodItemAdapter extends RecyclerView.Adapter<FoodItemAdapter.FoodItemViewHolder> {

    private List<Items> mItemList;
    private int type;
    private AddItemCallback mItemCallback;

    public FoodItemAdapter(List<Items> mItemList, AddItemCallback mItemCallback, int type) {
        this.type = type;
        this.mItemList = mItemList;
        this.mItemCallback = mItemCallback;
    }

    public void setItemList(List<Items> itemList)
    {
        this.mItemList=itemList;
    }

    @NonNull
    @Override
    public FoodItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food_layout, parent, false);
        return new FoodItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodItemViewHolder holder, int position) {
        if (type == 1) {
            if (mItemList.get(position).isAdded()) {
                holder.addItem.setText(R.string.added_to_cart_tag);
                holder.addItem.setEnabled(false);
            } else {
                holder.addItem.setText(R.string.add_item_tag);
                holder.addItem.setEnabled(true);
            }
        } else {
            if (mItemList.get(position).isAdded()) {
                holder.addItem.setText(R.string.mark_complete_tag);
            }
        }
        if(mItemList.get(position).getItemCurrentAmount()!=null) {
            holder.itemAmount.setText(String.format("₹ %s", mItemList.get(position).getItemCurrentAmount().toString()));
        }
        holder.itemName.setText(mItemList.get(position).getItemName());
        holder.itemQuantity.setText(mItemList.get(position).getItemQuantity());
        holder.itemImage.setImageResource(mItemList.get(position).getItemImage());
        holder.addItem.setOnClickListener(v -> {
            mItemCallback.updateItem(mItemList.get(position), type);
            if (type == 1) {
                mItemList.get(position).setAdded(true);
                notifyItemChanged(position);
            } else {
                mItemList.get(position).setAdded(false);
                mItemList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    static class FoodItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView itemImage;
        private TextView itemName, itemQuantity, itemAmount;
        private Button addItem;

        FoodItemViewHolder(View view) {
            super(view);
            itemImage = view.findViewById(R.id.item_image);
            itemName = view.findViewById(R.id.item_name);
            itemQuantity = view.findViewById(R.id.item_quantity);
            itemAmount = view.findViewById(R.id.item_amount);
            addItem = view.findViewById(R.id.add_item);
        }
    }
}
